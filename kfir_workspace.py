import random
import sys
import os
import copy
import matplotlib.pyplot as plt
import math

# GLOBALS
MAX_STEPS = 10000
trace = [0] * MAX_STEPS

expected_outputs = [0x63, 0xca, 0xb7, 0x04, 0x09, 0x53, 0xd0, 0x51, 0xcd, 0x60, 0xe0, 0xe7, 0xba, 0x70, 0xe1, 0x8c]

MIN_JUNCT_LOAD_CAPACITANCE = 0
MAX_JUNCT_LOAD_CAPACITANCE = 0
DELAY_MULT = 1
nets = dict()
nets_original = None
gates = dict()
last_step = 0


def net_set_val(net_name, new_net_val):
    nets[net_name]['val'] = new_net_val


def net_get_val(net_name):
    return nets[net_name]['val']


def net_is_valid(net_val):
    if net_val == 0:
        return True
    elif net_val == 1:
        return True
    else:
        return False


def net_get_load(net_name):
    return nets[net_name]['load']


def net_set_load(net_name, new_net_load):
    nets[net_name]['load'] = new_net_load


def net_do_step(output_net_name, target_val, step_count):
    delta = 1 / (DELAY_MULT * net_get_load(output_net_name))
    curr_val = net_get_val(output_net_name)
    new_val = curr_val + delta if target_val == 1 else curr_val - delta
    new_val = 1 if new_val > 1 else new_val
    new_val = 0 if new_val < 0 else new_val
    net_set_val(output_net_name, new_val)
    if new_val == 0:
        return 1
    if new_val == 1:
        return 1

    if delta > 0:
        trace[step_count] += 1

    return 0


def gate_do_op(gate_name, input_nets_names):
    gate_op = gates[gate_name]['type']
    if gate_op == 'inv':
        return 1 - int(net_get_val(input_nets_names[0]))
    elif gate_op == 'xor':
        result = 0
        for input_net_name in input_nets_names:
            result ^= int(net_get_val(input_net_name))
        return result
    elif gate_op == 'nor':
        result = 0
        for input_net_name in input_nets_names:
            result |= int(net_get_val(input_net_name))
        return 1 - result
    elif gate_op == 'and':
        result = int(net_get_val(input_nets_names[0]))
        for input_net_name in input_nets_names:
            result &= int(net_get_val(input_net_name))
        return result
    elif gate_op == 'or':
        result = 0
        for input_net_name in input_nets_names:
            # print('input_net_name = ', input_net_name)
            # print('net_get_val(input_net_name) = ', net_get_val((input_net_name)))
            result |= int(net_get_val(input_net_name))
        return result


def gate_do_step(gate_name, input_nets_names, output_net_name, step_count):
    is_all_ins_valid = True
    for input_name in input_nets_names:
        if net_is_valid(net_get_val(input_name)) is False:
            is_all_ins_valid = False

    if is_all_ins_valid is False:
        return False

    target_val = gate_do_op(gate_name, input_nets_names)
    return net_do_step(output_net_name, target_val, step_count)


def run_s_box():
    global MIN_JUNCT_LOAD_CAPACITANCE
    global MAX_JUNCT_LOAD_CAPACITANCE
    global last_step
    global trace
    # config
    randomize_junction_capacitance = 0
    randomize_junction_power = 0
    continous_model = 1
    MIN_JUNCT_LOAD_CAPACITANCE = 10
    MAX_JUNCT_LOAD_CAPACITANCE = 10

    # choosing key
    key_actual = 0xa
    print('key_actual = ', key_actual)

    init_s_box_hw()

    # config based variables
    if randomize_junction_capacitance == 1:
        num_traces = 5
        min_junct_noise = 1
        max_junct_noise = 3
        min_junct_power_noise_percent = 0
        max_junct_power_noise_percent = 0
    elif randomize_junction_power == 1:
        num_traces = 5
        min_junct_noise = 0
        max_junct_noise = 0
        min_junct_power_noise_percent = 0
        max_junct_power_noise_percent = 0
    else:
        num_traces = 1
        min_junct_power_noise_percent = 0
        max_junct_power_noise_percent = 0
        min_junct_noise = 0
        max_junct_noise = 0

    print('DELAY_MULT = ', DELAY_MULT)
    print('MIN_JUNCT_LOAD_CAPACITANCE = ', MIN_JUNCT_LOAD_CAPACITANCE, ' MAX_JUNCT_LOAD_CAPACITANCE = ', MAX_JUNCT_LOAD_CAPACITANCE)
    print('min_junct_noise = ', min_junct_noise, ' max_junct_noise = ', max_junct_noise)
    print('min_junct_power_noise_percent = ', min_junct_power_noise_percent, ' max_junct_power_noise_percent = ', max_junct_power_noise_percent)

    traces_summary = [[0] * num_traces for _ in range(16)]
    for in_val in range(16):
        for trace_num in range(num_traces):
            xored_message = key_actual ^ in_val
            init_s_box_inputs(xored_message)
            add_scalar_junction_noise(min_junct_noise, max_junct_noise)
            s_box_res = run_s_box_single_time()
            if randomize_junction_power == 1:
                trace = [(trace_val + trace_val * random.randint(min_junct_power_noise_percent, max_junct_power_noise_percent) / 100) for trace_val in
                         trace]
            print('in_val', in_val, 'result: ', hex(int(s_box_res)), 'total power: ', sum(trace))
            if continous_model == 0:
                traces_summary[in_val][trace_num] = sum(trace)
            else:
                traces_summary[in_val][trace_num] = copy.deepcopy(trace)
            reset_s_box_hw()

    print('traces_summary:\n', traces_summary)

    for key_guess in range(16):
        even_lsb = []
        odd_lsb = []
        for power_trace in range(16):
            input_guess = key_guess ^ power_trace
            output_guess = expected_outputs[input_guess]
            if output_guess % 2 == 0:
                for trace_val in traces_summary[power_trace]:
                    even_lsb.append(trace_val)
            else:
                for trace_val in traces_summary[power_trace]:
                    odd_lsb.append(trace_val)

        if continous_model == 0:
            even_avg = sum(even_lsb) / len(even_lsb)
            odd_avg = sum(odd_lsb) / len(odd_lsb)
            diff_avg = odd_avg - even_avg
            print('key_guess = ', key_guess, 'even_avg = ', even_avg, ' odd_avg = ', odd_avg, ' diff_avg = ', diff_avg)
        else:
            even_avg = [0] * MAX_STEPS
            for trace_val in even_lsb:
                even_avg = [even_avg[i] + trace_val[i] for i in range(len(trace_val))]
            even_avg = [even_avg[i] / len(even_lsb) for i in range(len(even_avg))]

            odd_avg = [0] * MAX_STEPS
            for trace_val in odd_lsb:
                odd_avg = [odd_avg[i] + trace_val[i] for i in range(len(trace_val))]
            odd_avg = [odd_avg[i] / len(odd_lsb) for i in range(len(odd_avg))]

            diff_avg = [odd_avg[i] - even_avg[i] for i in range(len(even_avg))]
            plt.figure(1)
            plt.subplot(4, 4, key_guess + 1)
            plt.plot(diff_avg)
            plt.axis([0, last_step, min(diff_avg) - 0.01, max(diff_avg) + 0.01])
            title_str = 'key guess = ' + str(key_guess)
            plt.title(title_str)
            plt.show()


def run_s_box_single_time():
    step_count = 0
    is_all_done = 0
    while is_all_done == 0:
        is_all_done = 1
        for i in range(0, 16):
            # BIT0xi first stage

            is_all_done &= gate_do_step('COMP' + hex(i) + '_xor_bit0', ['in0_bit0', 'COMP' + hex(i) + '_const_val_bit0'],
                                        'COMP' + hex(i) + '_xor_res_bit0', step_count)
            is_all_done &= gate_do_step('COMP' + hex(i) + '_xor_bit1', ['in0_bit1', 'COMP' + hex(i) + '_const_val_bit1'],
                                        'COMP' + hex(i) + '_xor_res_bit1', step_count)
            is_all_done &= gate_do_step('COMP' + hex(i) + '_xor_bit2', ['in0_bit2', 'COMP' + hex(i) + '_const_val_bit2'],
                                        'COMP' + hex(i) + '_xor_res_bit2', step_count)
            is_all_done &= gate_do_step('COMP' + hex(i) + '_xor_bit3', ['in0_bit3', 'COMP' + hex(i) + '_const_val_bit3'],
                                        'COMP' + hex(i) + '_xor_res_bit3', step_count)

        for i in range(0, 16):
            # BIT0xi second stage
            is_all_done &= gate_do_step('COMP' + hex(i) + '_or0', ['COMP' + hex(i) + '_xor_res_bit0', 'COMP' + hex(i) + '_xor_res_bit1'],
                                        'COMP' + hex(i) + '_or0_res_bit0', step_count)
            is_all_done &= gate_do_step('COMP' + hex(i) + '_or1', ['COMP' + hex(i) + '_xor_res_bit2', 'COMP' + hex(i) + '_xor_res_bit3'],
                                        'COMP' + hex(i) + '_or1_res_bit0', step_count)

        for i in range(0, 16):
            # BIT0xi third stage
            is_all_done &= gate_do_step('COMP' + hex(i) + '_nor0', ['COMP' + hex(i) + '_or0_res_bit0', 'COMP' + hex(i) + '_or1_res_bit0'],
                                        'COMP' + hex(i) + '_nor0_res_bit0', step_count)

        for i in range(0, 16):
            # BIT0xi fourth stage
            is_all_done &= gate_do_step('AND' + hex(i) + '_and_bit0', ['COMP' + hex(i) + '_nor0_res_bit0', 'AND' + hex(i) + '_const_val_bit0'],
                                        'AND' + hex(i) + '_res_bit0', step_count)
            is_all_done &= gate_do_step('AND' + hex(i) + '_and_bit1', ['COMP' + hex(i) + '_nor0_res_bit0', 'AND' + hex(i) + '_const_val_bit1'],
                                        'AND' + hex(i) + '_res_bit1', step_count)
            is_all_done &= gate_do_step('AND' + hex(i) + '_and_bit2', ['COMP' + hex(i) + '_nor0_res_bit0', 'AND' + hex(i) + '_const_val_bit2'],
                                        'AND' + hex(i) + '_res_bit2', step_count)
            is_all_done &= gate_do_step('AND' + hex(i) + '_and_bit3', ['COMP' + hex(i) + '_nor0_res_bit0', 'AND' + hex(i) + '_const_val_bit3'],
                                        'AND' + hex(i) + '_res_bit3', step_count)
            is_all_done &= gate_do_step('AND' + hex(i) + '_and_bit4', ['COMP' + hex(i) + '_nor0_res_bit0', 'AND' + hex(i) + '_const_val_bit4'],
                                        'AND' + hex(i) + '_res_bit4', step_count)
            is_all_done &= gate_do_step('AND' + hex(i) + '_and_bit5', ['COMP' + hex(i) + '_nor0_res_bit0', 'AND' + hex(i) + '_const_val_bit5'],
                                        'AND' + hex(i) + '_res_bit5', step_count)
            is_all_done &= gate_do_step('AND' + hex(i) + '_and_bit6', ['COMP' + hex(i) + '_nor0_res_bit0', 'AND' + hex(i) + '_const_val_bit6'],
                                        'AND' + hex(i) + '_res_bit6', step_count)
            is_all_done &= gate_do_step('AND' + hex(i) + '_and_bit7', ['COMP' + hex(i) + '_nor0_res_bit0', 'AND' + hex(i) + '_const_val_bit7'],
                                        'AND' + hex(i) + '_res_bit7', step_count)

        # level 0 of output ORs
        for output_bit_num in range(0, 8):  # iterating output bits
            for level0_or_num in range(0, 8):
                is_all_done &= gate_do_step('OR' + str(output_bit_num) + '_level0_or' + str(level0_or_num),
                                            ['AND' + hex(2 * level0_or_num) + '_res_bit' + str(output_bit_num),
                                             'AND' + hex(2 * level0_or_num + 1) + '_res_bit' + str(output_bit_num)],
                                            'OR' + str(output_bit_num) + '_level0_or' + str(level0_or_num) + '_res',
                                            step_count)

        # level 1 of output ORs
        for output_bit_num in range(0, 8):  # iterating output bits
            for level1_or_num in range(0, 4):
                is_all_done &= gate_do_step('OR' + str(output_bit_num) + '_level1_or' + str(level1_or_num),
                                            ['OR' + str(output_bit_num) + '_level0_or' + str(2 * level1_or_num) + '_res',
                                             'OR' + str(output_bit_num) + '_level0_or' + str(2 * level1_or_num + 1) + '_res'],
                                            'OR' + str(output_bit_num) + '_level1_or' + str(level1_or_num) + '_res',
                                            step_count)

        # level 2 of output ORs
        for output_bit_num in range(0, 8):  # iterating output bits
            for level2_or_num in range(0, 2):
                is_all_done &= gate_do_step('OR' + str(output_bit_num) + '_level2_or' + str(level2_or_num),
                                            ['OR' + str(output_bit_num) + '_level1_or' + str(2 * level2_or_num) + '_res',
                                             'OR' + str(output_bit_num) + '_level1_or' + str(2 * level2_or_num + 1) + '_res'],
                                            'OR' + str(output_bit_num) + '_level2_or' + str(level2_or_num) + '_res',
                                            step_count)

        # level 3 of output ORs
        for output_bit_num in range(0, 8):  # iterating output bits
            is_all_done &= gate_do_step('OR' + str(output_bit_num) + '_level3_or0',
                                        ['OR' + str(output_bit_num) + '_level2_or0_res',
                                         'OR' + str(output_bit_num) + '_level2_or1_res'],
                                        'OR' + str(output_bit_num) + '_level3_or0_res',
                                        step_count)

        # print('step_count:', step_count, 'nets:', nets, 'current trace:', trace[step_count])
        step_count += 1

    s_box_res = get_s_box_res()
    global last_step
    last_step = step_count
    return s_box_res


def get_s_box_res():
    res = net_get_val('OR7_level3_or0_res') * 128 + \
          net_get_val('OR6_level3_or0_res') * 64 + \
          net_get_val('OR5_level3_or0_res') * 32 + \
          net_get_val('OR4_level3_or0_res') * 16 + \
          net_get_val('OR3_level3_or0_res') * 8 + \
          net_get_val('OR2_level3_or0_res') * 4 + \
          net_get_val('OR1_level3_or0_res') * 2 + \
          net_get_val('OR0_level3_or0_res') * 1
    return res


def init_s_box_inputs(val):
    s_box_input = val
    in0_bit0 = s_box_input % 2
    in0_bit1 = int(s_box_input / 2) % 2
    in0_bit2 = int(s_box_input / 4) % 2
    in0_bit3 = int(s_box_input / 8) % 2
    net_set_val('in0_bit0', in0_bit0)
    net_set_val('in0_bit1', in0_bit1)
    net_set_val('in0_bit2', in0_bit2)
    net_set_val('in0_bit3', in0_bit3)


def reset_s_box_hw():
    # resetting trace
    for i in range(len(trace)):
        trace[i] = 0

    global nets_original
    global nets
    nets = copy.deepcopy(nets_original)


def init_s_box_hw():
    global MIN_JUNCT_LOAD_CAPACITANCE
    global MAX_JUNCT_LOAD_CAPACITANCE
    step_count = 0
    nets['in0_bit0'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['in0_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['in0_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['in0_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['COMP0x0_const_val_bit0'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x0_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x0_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x0_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['COMP0x1_const_val_bit0'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x1_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x1_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x1_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['COMP0x2_const_val_bit0'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x2_const_val_bit1'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x2_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x2_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['COMP0x3_const_val_bit0'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x3_const_val_bit1'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x3_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x3_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['COMP0x4_const_val_bit0'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x4_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x4_const_val_bit2'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x4_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['COMP0x5_const_val_bit0'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x5_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x5_const_val_bit2'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x5_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['COMP0x6_const_val_bit0'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x6_const_val_bit1'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x6_const_val_bit2'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x6_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['COMP0x7_const_val_bit0'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x7_const_val_bit1'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x7_const_val_bit2'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x7_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['COMP0x8_const_val_bit0'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x8_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x8_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x8_const_val_bit3'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['COMP0x9_const_val_bit0'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x9_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x9_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0x9_const_val_bit3'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['COMP0xa_const_val_bit0'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xa_const_val_bit1'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xa_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xa_const_val_bit3'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['COMP0xb_const_val_bit0'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xb_const_val_bit1'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xb_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xb_const_val_bit3'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['COMP0xc_const_val_bit0'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xc_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xc_const_val_bit2'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xc_const_val_bit3'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['COMP0xd_const_val_bit0'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xd_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xd_const_val_bit2'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xd_const_val_bit3'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['COMP0xe_const_val_bit0'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xe_const_val_bit1'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xe_const_val_bit2'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xe_const_val_bit3'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['COMP0xf_const_val_bit0'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xf_const_val_bit1'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xf_const_val_bit2'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['COMP0xf_const_val_bit3'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    # loading constants for AND gates
    nets['AND0x0_const_val_bit0'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x0_const_val_bit1'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x0_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x0_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x0_const_val_bit4'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x0_const_val_bit5'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x0_const_val_bit6'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x0_const_val_bit7'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['AND0x1_const_val_bit0'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x1_const_val_bit1'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x1_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x1_const_val_bit3'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x1_const_val_bit4'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x1_const_val_bit5'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x1_const_val_bit6'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x1_const_val_bit7'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['AND0x2_const_val_bit0'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x2_const_val_bit1'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x2_const_val_bit2'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x2_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x2_const_val_bit4'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x2_const_val_bit5'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x2_const_val_bit6'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x2_const_val_bit7'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['AND0x3_const_val_bit0'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x3_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x3_const_val_bit2'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x3_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x3_const_val_bit4'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x3_const_val_bit5'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x3_const_val_bit6'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x3_const_val_bit7'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['AND0x4_const_val_bit0'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x4_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x4_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x4_const_val_bit3'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x4_const_val_bit4'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x4_const_val_bit5'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x4_const_val_bit6'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x4_const_val_bit7'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['AND0x5_const_val_bit0'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x5_const_val_bit1'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x5_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x5_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x5_const_val_bit4'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x5_const_val_bit5'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x5_const_val_bit6'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x5_const_val_bit7'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['AND0x6_const_val_bit0'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x6_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x6_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x6_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x6_const_val_bit4'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x6_const_val_bit5'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x6_const_val_bit6'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x6_const_val_bit7'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['AND0x7_const_val_bit0'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x7_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x7_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x7_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x7_const_val_bit4'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x7_const_val_bit5'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x7_const_val_bit6'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x7_const_val_bit7'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['AND0x8_const_val_bit0'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x8_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x8_const_val_bit2'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x8_const_val_bit3'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x8_const_val_bit4'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x8_const_val_bit5'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x8_const_val_bit6'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x8_const_val_bit7'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['AND0x9_const_val_bit0'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x9_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x9_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x9_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x9_const_val_bit4'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x9_const_val_bit5'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x9_const_val_bit6'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0x9_const_val_bit7'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['AND0xa_const_val_bit0'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xa_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xa_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xa_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xa_const_val_bit4'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xa_const_val_bit5'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xa_const_val_bit6'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xa_const_val_bit7'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['AND0xb_const_val_bit0'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xb_const_val_bit1'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xb_const_val_bit2'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xb_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xb_const_val_bit4'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xb_const_val_bit5'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xb_const_val_bit6'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xb_const_val_bit7'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['AND0xc_const_val_bit0'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xc_const_val_bit1'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xc_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xc_const_val_bit3'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xc_const_val_bit4'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xc_const_val_bit5'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xc_const_val_bit6'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xc_const_val_bit7'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['AND0xd_const_val_bit0'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xd_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xd_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xd_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xd_const_val_bit4'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xd_const_val_bit5'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xd_const_val_bit6'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xd_const_val_bit7'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['AND0xe_const_val_bit0'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xe_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xe_const_val_bit2'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xe_const_val_bit3'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xe_const_val_bit4'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xe_const_val_bit5'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xe_const_val_bit6'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xe_const_val_bit7'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    nets['AND0xf_const_val_bit0'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xf_const_val_bit1'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xf_const_val_bit2'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xf_const_val_bit3'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xf_const_val_bit4'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xf_const_val_bit5'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xf_const_val_bit6'] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
    nets['AND0xf_const_val_bit7'] = {'val': 1, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    for i in range(0, 16):
        # implementing comparators for in0 and vals 4'h0...F
        net_name_xor_res_bit0 = 'COMP' + hex(i) + '_xor_res_bit0'
        net_name_xor_res_bit1 = 'COMP' + hex(i) + '_xor_res_bit1'
        net_name_xor_res_bit2 = 'COMP' + hex(i) + '_xor_res_bit2'
        net_name_xor_res_bit3 = 'COMP' + hex(i) + '_xor_res_bit3'
        net_name_or0_res_bit0 = 'COMP' + hex(i) + '_or0_res_bit0'
        net_name_or1_res_bit0 = 'COMP' + hex(i) + '_or1_res_bit0'
        net_name_nor0_res_bit0 = 'COMP' + hex(i) + '_nor0_res_bit0'

        nets[net_name_xor_res_bit0] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
        nets[net_name_xor_res_bit1] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
        nets[net_name_xor_res_bit2] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
        nets[net_name_xor_res_bit3] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
        nets[net_name_or0_res_bit0] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
        nets[net_name_or1_res_bit0] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
        nets[net_name_nor0_res_bit0] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

        # loading outputs of 8 bit AND gate for in0
        net_name_and_res_bit0 = 'AND' + hex(i) + '_res_bit0'
        net_name_and_res_bit1 = 'AND' + hex(i) + '_res_bit1'
        net_name_and_res_bit2 = 'AND' + hex(i) + '_res_bit2'
        net_name_and_res_bit3 = 'AND' + hex(i) + '_res_bit3'
        net_name_and_res_bit4 = 'AND' + hex(i) + '_res_bit4'
        net_name_and_res_bit5 = 'AND' + hex(i) + '_res_bit5'
        net_name_and_res_bit6 = 'AND' + hex(i) + '_res_bit6'
        net_name_and_res_bit7 = 'AND' + hex(i) + '_res_bit7'

        nets[net_name_and_res_bit0] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
        nets[net_name_and_res_bit1] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
        nets[net_name_and_res_bit2] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
        nets[net_name_and_res_bit3] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
        nets[net_name_and_res_bit4] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
        nets[net_name_and_res_bit5] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
        nets[net_name_and_res_bit6] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}
        nets[net_name_and_res_bit7] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

        # gates needed for comparator
        gate_name_xor_bit0 = 'COMP' + hex(i) + '_xor_bit0'
        gate_name_xor_bit1 = 'COMP' + hex(i) + '_xor_bit1'
        gate_name_xor_bit2 = 'COMP' + hex(i) + '_xor_bit2'
        gate_name_xor_bit3 = 'COMP' + hex(i) + '_xor_bit3'
        gate_name_or0 = 'COMP' + hex(i) + '_or0'
        gate_name_or1 = 'COMP' + hex(i) + '_or1'
        gate_name_nor0 = 'COMP' + hex(i) + '_nor0'

        gates[gate_name_xor_bit0] = {'type': 'xor', 'num_ins': 2}
        gates[gate_name_xor_bit1] = {'type': 'xor', 'num_ins': 2}
        gates[gate_name_xor_bit2] = {'type': 'xor', 'num_ins': 2}
        gates[gate_name_xor_bit3] = {'type': 'xor', 'num_ins': 2}
        gates[gate_name_or0] = {'type': 'or', 'num_ins': 2}
        gates[gate_name_or1] = {'type': 'or', 'num_ins': 2}
        gates[gate_name_nor0] = {'type': 'nor', 'num_ins': 2}

        # gates needed for 8 bit AND gate
        gate_name_and_bit0 = 'AND' + hex(i) + '_and_bit0'
        gate_name_and_bit1 = 'AND' + hex(i) + '_and_bit1'
        gate_name_and_bit2 = 'AND' + hex(i) + '_and_bit2'
        gate_name_and_bit3 = 'AND' + hex(i) + '_and_bit3'
        gate_name_and_bit4 = 'AND' + hex(i) + '_and_bit4'
        gate_name_and_bit5 = 'AND' + hex(i) + '_and_bit5'
        gate_name_and_bit6 = 'AND' + hex(i) + '_and_bit6'
        gate_name_and_bit7 = 'AND' + hex(i) + '_and_bit7'

        gates[gate_name_and_bit0] = {'type': 'and', 'num_ins': 2}
        gates[gate_name_and_bit1] = {'type': 'and', 'num_ins': 2}
        gates[gate_name_and_bit2] = {'type': 'and', 'num_ins': 2}
        gates[gate_name_and_bit3] = {'type': 'and', 'num_ins': 2}
        gates[gate_name_and_bit4] = {'type': 'and', 'num_ins': 2}
        gates[gate_name_and_bit5] = {'type': 'and', 'num_ins': 2}
        gates[gate_name_and_bit6] = {'type': 'and', 'num_ins': 2}
        gates[gate_name_and_bit7] = {'type': 'and', 'num_ins': 2}




        # END of for loop

    # output OR between all 16 AND outputs
    for output_bit_num in range(0, 8):  # iterating output bits
        for level0_or_num in range(0, 8):
            gate_name = 'OR' + str(output_bit_num) + '_level0_or' + str(level0_or_num)
            gates[gate_name] = {'type': 'or', 'num_ins': 2}
            net_name = 'OR' + str(output_bit_num) + '_level0_or' + str(level0_or_num) + '_res'
            nets[net_name] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

        for level1_or_num in range(0, 4):
            gate_name = 'OR' + str(output_bit_num) + '_level1_or' + str(level1_or_num)
            gates[gate_name] = {'type': 'or', 'num_ins': 2}
            net_name = 'OR' + str(output_bit_num) + '_level1_or' + str(level1_or_num) + '_res'
            nets[net_name] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

        for level2_or_num in range(0, 2):
            gate_name = 'OR' + str(output_bit_num) + '_level2_or' + str(level2_or_num)
            gates[gate_name] = {'type': 'or', 'num_ins': 2}
            net_name = 'OR' + str(output_bit_num) + '_level2_or' + str(level2_or_num) + '_res'
            nets[net_name] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

        for level3_or_num in range(0, 8):
            gate_name = 'OR' + str(output_bit_num) + '_level3_or' + str(level3_or_num)
            gates[gate_name] = {'type': 'or', 'num_ins': 2}
            net_name = 'OR' + str(output_bit_num) + '_level3_or' + str(level3_or_num) + '_res'
            nets[net_name] = {'val': 0, 'load': random.randint(MIN_JUNCT_LOAD_CAPACITANCE, MAX_JUNCT_LOAD_CAPACITANCE)}

    # saving original nets with their load capacitance
    global nets_original
    nets_original = copy.deepcopy(nets)


def add_scalar_junction_noise(min_noise, max_noise):
    global nets
    for net in nets:
        new_net_load_capacitance = net_get_load(net) + random.randint(min_noise, max_noise)
        net_set_load(net, new_net_load_capacitance)


def example_circuit():
    step_count = 0
    nets['A'] = {'val': 0, 'load': 10}
    nets['B'] = {'val': 0, 'load': 2}
    nets['nA'] = {'val': 0, 'load': 5}
    nets['nB'] = {'val': 0, 'load': 3}
    nets['C'] = {'val': 0, 'load': 4}
    nets['D'] = {'val': 0, 'load': 5}
    nets['Y'] = {'val': 0, 'load': 6}

    gates['inv1'] = {'type': 'inv', 'num_ins': 1}
    gates['inv2'] = {'type': 'inv', 'num_ins': 1}
    gates['and1'] = {'type': 'and', 'num_ins': 2}
    gates['and2'] = {'type': 'and', 'num_ins': 2}
    gates['or1'] = {'type': 'or', 'num_ins': 2}

    net_set_val('A', 1)
    is_all_done = 0
    while is_all_done == 0:
        is_all_done = 1
        is_all_done &= gate_do_step('inv1', ['A'], 'nA', step_count)
        is_all_done &= gate_do_step('inv2', ['B'], 'nB', step_count)
        is_all_done &= gate_do_step('and1', ['A', 'nB'], 'C', step_count)
        is_all_done &= gate_do_step('and2', ['nA', 'B'], 'D', step_count)
        is_all_done &= gate_do_step('or1', ['C', 'D'], 'Y', step_count)

        print('step_count:', step_count, 'nets:', nets, 'current trace:', trace[step_count])
        step_count += 1


def main():
    # example_circuit()
    run_s_box()
    # print('The trace is:')
    # print(trace)
    print('done')


if __name__ == '__main__':
    main()
